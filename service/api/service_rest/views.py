from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from django.db import IntegrityError
from .models import Technician, AutomobileVO, Appointment
from common.json import ModelEncoder

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]



@require_http_methods(["GET", "POST"])
def api_list_technicians(request, employee_id=None):

    if request.method == "GET":
        if employee_id is not None:
            technicians = Technician.objects.filter(employee_id=employee_id)
            return JsonResponse(
                {"technicians": technicians},
                encoder=TechnicianEncoder,
            )
        else:
            technicians = Technician.objects.all()
            return JsonResponse(
                {"technicians": technicians},
                encoder=TechnicianEncoder,
            )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            employee_id = content.get("employee_id")
            if employee_id is None:
                raise Technician.DoesNotExist

            technician = Technician.objects.create(**content)
            return JsonResponse(
                {"technician": technician},
                encoder=TechnicianEncoder,
                safe=False,
            )
        except IntegrityError:
            return JsonResponse(
                {"message": "Duplicate employee_id. This employee_id is already in use."},
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee id"},
            )


@require_http_methods(["DELETE", "GET"])
def api_show_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(employee_id=id)
        return JsonResponse(
            technician,
            encoder= TechnicianEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(employee_id=id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder= TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message":"That technician does not exist"})


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "sold"]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }

@require_http_methods(["GET", "POST", "DELETE"])
def api_list_appointments(request, id=None):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointment},
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = content["technician"]
            technician = Technician.objects.get(id=technician)
            content["technician"] = technician

            appointment = Appointment.objects.create(**content)

            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create the appointment"})
            response.status_code = 400
            return response
    else:
        try:
            if id is not None:
                appointment = Appointment.objects.get(id=id)
                appointment.delete()
                response = JsonResponse({"message": "Delete successful"})
                response.status_code = 200
                return response
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does Not Exist"})
            response.status_code = 404
            return response
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False
    )




@require_http_methods(["DELETE", "GET"])
def api_show_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder= AppointmentEncoder,
                safe= False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message":"The selected appointment does not exist."})





require_http_methods(["PUT"])
def api_update_appointment(request, id, status):
    if request.method == "PUT":
        statuses = {
            "finish": "Finished",
            'cancel': "Cancelled",
        }
        appointment = Appointment.objects.get(id=id)
        appointment.status = statuses[status]
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    elif Appointment.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response
