from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=300, unique=True, null=True)
    sold = models.BooleanField(default=False)
    vin = models.CharField(max_length=300, unique=True, null=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField(unique=True)

    class Meta:
        ordering = ["employee_id"]


    def __str__(self):
        return self.first_name

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk":self.id})


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200, null=True)
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=200)

    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["date_time"]

    def __str__(self):
        return self.vin


    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})
