import React, { useState, useEffect } from 'react';

function VehicleForm() {
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: '',
  });

  const [successMessage, setSuccessMessage] = useState('');

  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    const fetchOptions = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    };

    fetch('http://localhost:8100/api/manufacturers/', fetchOptions)
      .then(res => res.json())
      .then(data => setManufacturers(data.manufacturers));
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setSuccessMessage('Vehicle model created successfully!');

      setFormData({
        name: '',
        picture_url: '',
        manufacturer_id: '',
      });
    }
  };

  const handleFormChange = (e) => {
    const { value, name } = e.target;

    setFormData({
      ...formData,
      [name]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Vehicle!</h1>
          {successMessage && <p style={{ color: 'green' }}>{successMessage}</p>}
          <form onSubmit={handleSubmit} id="create-vehicle-form">
            <div className="form-floating mb-3">
              <input
                value={formData.name}
                onChange={handleFormChange}
                placeholder="Vehicle Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Vehicle Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.picture_url}
                onChange={handleFormChange}
                placeholder="Picture URL"
                required
                type="text"
                name="picture_url"
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select name="manufacturer_id" value={formData.manufacturer_id} onChange={handleFormChange} className="form-select" required>
                <option value="">Choose a Manufacturer...</option>
                {manufacturers.map((manufacturer) => (
                  <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default VehicleForm;