import { useState, useEffect } from 'react';

function AppointmentsList() {
    const [appointments, setAppointments] = useState([])
    const [sold, setSold] = useState([]);


    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const { appointments } = await response.json();
            setAppointments(appointments);
        } else {
            console.error('An error occurred fetching the data')
        }
    }

    useEffect(() => {
        const getSoldAutos = async () => {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            if (response.ok) {
                const { autos } = await response.json();
                setSold( autos );
            } else {
                console.error('An error occured fetching automobile data.')
            }
        };

        getSoldAutos()
        getData()
    }, []);

    const isVipCustomer = (vin) => {
        return sold.some(auto => auto.vin === vin);
    }

    const formatDate = (dateTimeString) => {
        const dateDisplay = { hour: 'numeric', minute: 'numeric', day: 'numeric', month: 'long', year: 'numeric'};
        return new Date(dateTimeString).toLocaleDateString(undefined, dateDisplay);
    }


    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Service History</h1>
                <table className="table table-striped m-3">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>VIP Customer?</th>
                            <th>Customer</th>
                            <th>Date Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map(appointment => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{isVipCustomer(appointment.vin)? "Yes": "No"}</td>

                                    <td>{appointment.customer}</td>
                                    <td>{formatDate(appointment.date_time)}</td>
                                    <td>{appointment.technician.first_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.status}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
};


export default AppointmentsList;


///const filteredAppointmentsStatus = appointments.filter(appointment =>
///    appointment.status != 'canceled' && appointment.status != 'finished'
///);
