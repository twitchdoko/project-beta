import React, { useEffect, useState } from 'react';


function AppointmentForm() {
    const [formData, setFormData] = useState({
        date_time: '',
        reason: '',
        vin: '',
        customer: '',
        technician: '',
    });

    const [technicians, setTechnicians] = useState([]);

    const fetchOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    }

    useEffect(() => {
        fetch('http://localhost:8080/api/technicians/', fetchOptions)
            .then(res => res.json())
            .then(data => setTechnicians(data.technicians));
    }, []);




    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8080/api/appointments/";

        try {
            const response = await fetch(url, {
                method: 'POST',
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            console.log(formData)


            if (response.ok) {
                console.log('Appointment created successfully.');

                setFormData({
                    date_time: '',
                    reason: '',
                    vin: '',
                    customer: '',
                    technician: '',
                });



            } else {
                console.error(response.statusText)
            }
        } catch (error) {
            console.error('Error creating appointment', error);
        }
    };

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
    };


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an Appointment</h1>
                    <form onSubmit={handleSubmit} id="appointment-form">

                        <div className="form-floating mb-3">
                            <input type="text" name="vin" value={formData.vin} onChange={handleInputChange} className="form-control" required />
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input type="text" name="customer" value={formData.customer} onChange={handleInputChange}  className="form-control" required />
                            <label htmlFor="vin">Customer Name</label>
                        </div>


                        <div className="form-floating mb-3">
                            <input onChange={handleInputChange} value={formData.date_time} placeholder="date_time" required type="datetime-local" name="date_time" className="form-control" />
                            <label htmlFor="date_time">Date and Time</label>
                        </div>

                        <div className="mb-3">
                        <select name="technician" value={formData.technician} onChange={handleInputChange} className="form-select" required>
                            <option value="">Choose a technician...</option>
                            {technicians.map((technician) => (
                            <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                            ))}
                        </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" name="reason" value={formData.reason} onChange={handleInputChange}  className="form-control" required />
                            <label htmlFor="reason">Reson for service...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm;
