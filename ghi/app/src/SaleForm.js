import React, { useState, useEffect } from "react";

function SaleForm() {
  const [formData, setFormData] = useState({
    automobile: "",
    salesperson: "",
    customer: "",
    price: "",
  });

  const [autos, setAutomobiles] = useState([]);
  const [salespersons, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);

  const [successMessage, setSuccessMessage] = useState("");

  const fetchOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };

  useEffect(() => {
    fetch("http://localhost:8100/api/automobiles/", fetchOptions)
      .then((res) => res.json())
      .then((data) => setAutomobiles(data.autos));
    fetch("http://localhost:8090/api/customers/", fetchOptions)
      .then((res) => res.json())
      .then((data) => setCustomers(data.customers));
    fetch("http://localhost:8090/api/salespeople/", fetchOptions)
      .then((res) => res.json())
      .then((data) => setSalespeople(data.salespersons));
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8090/api/sales/";

    try {
      const response = await fetch(url, {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (response.ok) {
        setSuccessMessage("Sale created successfully!");

        try {
          const autoResponse = await fetch(
            `http://localhost:8100/api/automobiles/${formData.automobile}/`,
            {
              method: "PUT",
              body: JSON.stringify({ sold: true }),
              headers: {
                "Content-Type": "application/json",
              },
            }
          );
          if (autoResponse.ok) {
            const updatedAutos = autos.filter(
              (auto) => auto.vin !== formData.automobile
            );
            setAutomobiles(updatedAutos);
          } else {
            console.error(autoResponse.statusText);
          }
        } catch (error) {
          console.error("Error updating automobile sold property:", error);
        }

        setFormData({
          automobile: "",
          salesperson: "",
          customer: "",
          price: "",
        });
      } else {
        console.error(response.statusText);
      }
    } catch (error) {
      console.error("Error recording sale:", error);
    }
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          {successMessage && <p style={{ color: "green" }}>{successMessage}</p>}
          <form onSubmit={handleSubmit} id="sale-form">
            <div className="mb-3">
              <select
                name="automobile"
                value={formData.automobile}
                onChange={handleInputChange}
                className="form-select"
                required
              >
                <option value="">Choose an automobile VIN...</option>
                {autos
                  .filter((auto) => !auto.sold)
                  .map((auto) => (
                    <option key={auto.vin} value={auto.vin}>
                      {auto.vin}
                    </option>
                  ))}
              </select>
            </div>
            <div className="mb-3">
              <select
                name="salesperson"
                value={formData.salesperson}
                onChange={handleInputChange}
                className="form-select"
                required
              >
                <option value="">Choose a salesperson...</option>
                {salespersons.map((salesperson) => (
                  <option key={salesperson.id} value={salesperson.id}>
                    {salesperson.first_name} {salesperson.last_name}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <select
                name="customer"
                value={formData.customer}
                onChange={handleInputChange}
                className="form-select"
                required
              >
                <option value="">Choose a customer...</option>
                {customers.map((customer) => (
                  <option key={customer.id} value={customer.id}>
                    {customer.first_name} {customer.last_name}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                id="price"
                type="number"
                name="price"
                value={formData.price}
                onChange={handleInputChange}
                placeholder="Price"
                className="form-control"
                required
              />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SaleForm;
