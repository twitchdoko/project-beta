import React, { useState, useEffect } from "react";

function AutomobileForm() {
  const [formData, setFormData] = useState({
    color: "",
    year: "",
    vin: "",
    model_id: "",
  });

  const [models, setModels] = useState([]);

  const fetchData = async () => {
    const modelUrl = "http://localhost:8100/api/models/";
    const response = await fetch(modelUrl);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const [successMessage, setSuccessMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8100/api/automobiles/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setSuccessMessage("Automobile created successfully!");

      setFormData({
        color: "",
        year: "",
        vin: "",
        model_id: "",
      });
    }
  };

  const handleFormChange = (e) => {
    const { value, name } = e.target;

    if (name !== "model_id") {
      setFormData({
        ...formData,
        [name]: value,
      });
    } else {
      setFormData({
        ...formData,
        model_id: value,
      });
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create an Automobile</h1>
          {successMessage && <p style={{ color: "green" }}>{successMessage}</p>}
          <form onSubmit={handleSubmit} id="add-automobile">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.color}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.year}
                placeholder="Year"
                required
                type="text"
                name="year"
                id="year"
                className="form-control"
              />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.vin}
                placeholder="Vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
                maxLength={17}
              />
              <label htmlFor="vin">Vin</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleFormChange}
                value={formData.model_id}
                required
                name="model_id"
                id="model_id"
                className="form-select"
              >
                <option value="">Choose a Model...</option>
                {models.map((model) => {
                  return (
                    <option key={model.id} value={model.id}>
                      {model.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
