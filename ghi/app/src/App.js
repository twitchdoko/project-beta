import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonForm from './SalespersonForm';
import SalespeopleList from './ListSalespeople';
import CustomerForm from './CustomerForm';
import CustomerList from './ListCustomers';
import SaleForm from './SaleForm';
import SaleList from './ListSales';
import ManufacturerList from './ListManufacturers';
import ManufacturerForm from './ManufacturerForm';
import VehicleList from './ListVehicleModels';
import SalesHistory from './SalesHistory';
import VehicleForm from './VehicleForm';
import TechnicianList from './ListTechnicians';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import ListAppointments from './ListAppointments';
import HistoryAppointments from './HistoryAppoinments';
import AutomobileForm from './AutomobileForm';
import ListAutomobiles from './ListAutomobiles';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/automobiles/create" element={<AutomobileForm/>} />
          <Route path="/automobiles/" element={<ListAutomobiles/>} />
          <Route path="/models/create" element={<VehicleForm/>} />
          <Route path="/salespeople/history" element={<SalesHistory/>} />
          <Route path="/models/" element={<VehicleList/>} />
          <Route path="/manufacturers/create/" element={<ManufacturerForm/>} />
          <Route path ="/manufacturers/" element={<ManufacturerList/>} />
          <Route path="/sales/" element={<SaleList/>} />
          <Route path="/sales/create" element={<SaleForm/>} />
          <Route path="/customers/" element={<CustomerList/>} />
          <Route path="/customers/create" element={<CustomerForm/>} />
          <Route path="/salespeople/" element={<SalespeopleList/>} />
          <Route path="/salespeople/create" element={<SalespersonForm/>} />
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/technicians/" element={<TechnicianList />} />
          <Route path="/appointments/new" element={<AppointmentForm />} />
          <Route path="/appointments/" element={<ListAppointments />} />
          <Route path="/appointments/history" element={<HistoryAppointments />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
