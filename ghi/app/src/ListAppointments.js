import React, { useState, useEffect } from 'react';

function AppointmentsList() {
    const [appointments, setAppointments] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [sold, setSold] = useState([]);

    const getAppointmentData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const { appointments } = await response.json();
            setAppointments(appointments);

        } else {
            console.error('An error occurred fetching appointment data')
        }
    };

    useEffect(() => {
        const getSoldAutos = async () => {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            if (response.ok) {
                const { autos } = await response.json();
                setSold( autos );
            } else {
                console.error('An error occured fetching automobile data.')
            }
        };
        getSoldAutos();
        getAppointmentData();
    }, []);

    const handleSearchInputChange = (event) => {
        setSearchQuery(event.target.value);
    };

    const setAppointmentCancelled = async (id) => {
        await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
            method: 'PUT'
        });
        getAppointmentData();
    }

    const setAppointmentFinished = async (id) => {
        await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
            method: 'PUT'
        });
        getAppointmentData();
    }

    const isVipCustomer = (vin) => {
        return sold.some(auto => auto.vin === vin);
    }


    const filteredAppointmentsVin = appointments.filter(appointment =>
        appointment.vin.toLowerCase().includes(searchQuery.toLowerCase()) && appointment.status !== 'Finished'
    );

    const formatDate = (dateTimeString) => {
        const dateDisplay = { hour: 'numeric', minute: 'numeric', day: 'numeric', month: 'long', year: 'numeric'};
        return new Date(dateTimeString).toLocaleDateString(undefined, dateDisplay);
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Service Appointments</h1>
                <input
                    type="text"
                    placeholder="Search by VIN"
                    value={searchQuery}
                    onChange={handleSearchInputChange}
                    className="form-control m-3"
                />
                <table className="table table-striped m-3">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>VIP Customer?</th>
                            <th>Customer</th>
                            <th>Date & Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filteredAppointmentsVin.map(appointment => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{isVipCustomer(appointment.vin) ? "Yes" : "No"}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{formatDate(appointment.date_time)}</td>
                                    <td>{appointment.technician.first_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button className='btn btn-danger' onClick={ ()=> setAppointmentCancelled (appointment.id)}>Cancel</button>
                                        <button className='btn btn-success' onClick={ ()=> setAppointmentFinished (appointment.id)}>Finish</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default AppointmentsList;
