import React, { useEffect, useState } from 'react';

function TechnicianForm() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');


    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const { employee_id } = data;
            setEmployeeId ( employee_id )
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;




        const technicianUrl = `http://localhost:8080/api/technicians/`
        console.log(technicianUrl)
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const technicianResponse = await fetch(technicianUrl, fetchOptions);
        if (technicianResponse.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
    }

    const handleChangeFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleChangeLastName = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleChangeEmployeeId = (event) => {
        const value = parseInt(event.target.value, 10);
        if (!isNaN(value)) {
            setEmployeeId(value);
        }
    }


    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form className="mb-3" onSubmit={handleSubmit} id="create-technician-form">
                                <h1 className="card-title">Add a Technician </h1>
                                <div className="mb-3">
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input value={first_name} onChange={handleChangeFirstName} required placeholder="First Name" type="text" id="first_name" name="first_name" className="form-control" />
                                        <label htmlFor="first_name"> First Name </label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input value={last_name} onChange={handleChangeLastName} required placeholder="Last Name" type="text" id="last_name" name="last_name" className="form-control" />
                                        <label htmlFor="first_name"> Last Name </label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            value={employee_id}
                                            onChange={handleChangeEmployeeId}
                                            required
                                            placeholder="Last Name"
                                            id="employee_id"
                                            name="employee_id"
                                            className="form-control"
                                            maxLength={4}
                                            pattern="[0-9]*"
                                        />
                                        <label htmlFor="employee_id"> Employee ID Number :  Enter 4 digits </label>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );


}

export default TechnicianForm;
