import { useState, useEffect } from "react";

function SalesHistory() {
  const [sales, setSales] = useState([]);
  const [salespersons, setSalespeople] = useState([]);
  const [salesperson, setSalesperson] = useState("");

  const fetchOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };

  useEffect(() => {
    fetch("http://localhost:8090/api/sales/", fetchOptions)
      .then((res) => res.json())
      .then((data) => setSales(data.sales));
    fetch("http://localhost:8090/api/salespeople/", fetchOptions)
      .then((res) => res.json())
      .then((data) => setSalespeople(data.salespersons));
  }, []);

  const handleSelectChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  };

  return (
    <div className="container">
      <div className="container mb-3">
        <h1 className="mt-4">Sales History</h1>
        <select
          onChange={handleSelectChange}
          value={salesperson}
          required
          name="salespersons"
          id="salespersons"
          className="form-select"
        >
          <option value="">Choose a salesperson...</option>
          {salespersons.map((salespersons) => {
            return (
              <option key={salespersons.id} value={salespersons.id}>
                {salespersons.first_name} {salespersons.last_name}
              </option>
            );
          })}
        </select>
      </div>
      <div className="container mb-3">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {sales
              .filter((sale) => sale.salesperson_id === parseInt(salesperson))
              .map((sale) => {
                return (
                  <tr key={sale.id}>
                    <td>
                      {sale.salesperson_first_name} {sale.salesperson_last_name}
                    </td>
                    <td>
                      {sale.customer_first_name} {sale.customer_last_name}
                    </td>
                    <td>{sale.vin}</td>
                    <td>${sale.price}</td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default SalesHistory;
