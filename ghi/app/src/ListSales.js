import { useState, useEffect } from "react";

function SaleList() {
  const [sales, setSales] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const { sales } = await response.json();
      setSales(sales);
    } else {
      console.error("An error occurred fetching the data");
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="my-5 container">
      <div className="row">
        <h1>Sales</h1>
        <table className="table table-striped m-3">
          <thead>
            <tr>
              <th>Salesperson Employee ID</th>
              <th>Salesperson Name</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {sales.map((sales) => {
              return (
                <tr key={sales.id}>
                  <td>{sales.salesperson_employee_id}</td>
                  <td>
                    {sales.salesperson_first_name +
                      " " +
                      sales.salesperson_last_name}
                  </td>
                  <td>
                    {sales.customer_first_name + " " + sales.customer_last_name}
                  </td>
                  <td>{sales.vin}</td>
                  <td>{sales.price}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default SaleList;
