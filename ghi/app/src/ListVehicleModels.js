import { useState, useEffect } from "react";

function VehicleList() {
  const [models, setVehicles] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const { models } = await response.json();
      setVehicles(models);
    } else {
      console.error("An error occurred fetching the data");
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="my-5 container">
      <div className="row">
        <h1>Models</h1>
        <table className="table table-striped m-3">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {models.map((models) => {
              return (
                <tr key={models.id}>
                  <td>{models.name}</td>
                  <td>{models.manufacturer.name}</td>
                  <td>
                    <img
                      src={models.picture_url}
                      alt={`Model ${models.name}`}
                      style={{ maxWidth: "250px", maxHeight: "250px" }}
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default VehicleList;
