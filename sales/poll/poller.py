import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
from sales_rest.models import AutomobileVO


def get_autos():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    if response.status_code != 200:
        print(f"Unexpected status code: {response.status_code}")

    content = json.loads(response.content)

    autos_data = content.get("autos", [])

    for auto in autos_data:
        vin = auto["vin"]
        sold = auto["sold"]
        existing_auto = AutomobileVO.objects.filter(vin=vin).first()
        if existing_auto:
            existing_auto.sold = sold
            existing_auto.save()
        else:
            AutomobileVO.objects.create(
                import_href=auto["href"],
                vin=vin,
                sold=sold,
            )


def poll(repeat=True):
    while True:
        print("Sales poller polling for data")
        try:
            get_autos()

        except Exception as e:
            print(e, file=sys.stderr)

        if not repeat:
            break

        time.sleep(1)


if __name__ == "__main__":
    poll()
