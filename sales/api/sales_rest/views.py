import json
from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.core.exceptions import ValidationError

# Create your views here.


class SalesPersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "sold",
        "vin",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "customer_first_name": o.customer.first_name,
            "customer_last_name": o.customer.last_name,
            "customer_address": o.customer.address,
            "customer_phone_number": o.customer.phone_number,
            "salesperson_first_name": o.salesperson.first_name,
            "salesperson_last_name": o.salesperson.last_name,
            "salesperson_employee_id": o.salesperson.employee_id,
            "salesperson_id": o.salesperson.id,
            "vin": o.automobile.vin,
        }

    encoders = {
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        sales_person = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": sales_person},
            encoder=SalesPersonEncoder)
    else:
        content = json.loads(request.body)
    try:
        sales_person = Salesperson.objects.create(**content)
        return JsonResponse(sales_person,
                            encoder=SalesPersonEncoder,
                            safe=False)
    except ValidationError as e:
        return JsonResponse({"message": str(e)}, status=400)


@require_http_methods(["DELETE"])
def api_delete_salesperson(request, id):
    if request.method == "DELETE":
        try:
            sales_person = Salesperson.objects.get(id=id)
            sales_person.delete()
            return JsonResponse(sales_person,
                                encoder=SalesPersonEncoder,
                                safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except ValidationError as e:
            return JsonResponse({"message": str(e)}, status=400)


@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_sale(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({"sales": sales}, encoder=SaleEncoder)
    else:
        content = json.loads(request.body)
        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
            automobile.sold = True
            automobile.save()
        except AutomobileVO.DoesNotExist:
            response = JsonResponse({"message": "AutomobileVO does not exist"})
            response.status_code = 400
            return response
        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 400
            return response
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 400
            return response

    sale = Sale.objects.create(**content)
    return JsonResponse(
        sale,
        encoder=SaleEncoder,
        safe=False,
    )


@require_http_methods(["GET", "DELETE"])
def api_show_salesrecord(request, id):
    if request.method == "GET":
        try:
            sales_persons = Sale.objects.get(id=id)
            return JsonResponse(sales_persons, encoder=SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does no exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(sale, encoder=SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
