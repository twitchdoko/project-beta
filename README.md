# CarCar

CarCar is an application for managing the aspects of this automobile dealership. It manages the inventory, automobile sales, and automobile services.

Team:
    * Jordan Rubenstien - Auto Sales
    * Tyler Carreola - Auto Services


## To get started:

--Make sure you have Docker, Git, and Node.js 18.2 or above

        1. Fork this repository

        2. Clone the forked repository onto your local computer:
        git clone <<respository.url.here>>

        3. Build and run the project using Docker with these commands:
        ```
        docker volume create beta-data
        docker-compose build
        docker-compose up
        ```
        - After running these commands, make sure all of your Docker containers are running

        - View the project in the browser: http://localhost:3000/

## Design


![CarCar Diagram](CarCarReadME.png)

CarCar has 3 microservices which interact with each other:
    --Inventory
    --Services
    --Sales


Inventory API provides Automobile RESTful API endpoints. This is used to pull Automobile data.

The Services and Sales domains work together with our Inventory domain. Data starts at our inventory domain. We keep a record of automobiles on the lot that are available to buy. Our sales and service microservices obtain information from the inventory domain, using a **poller**, which talks to the inventory domain to keep track of which vehicles we have in our inventory so that the service and sales team always has up-to-date information.



################ Service Microservice ################


*** Appointments: ***

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Service appointment detail | GET | http://localhost:8080/api/appointments/<intid>/
| Service appointment status | PUT | http://localhost:8080/api/appointments/<intid>/<str:status>/
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Delete service appointment | DELETE | http://localhost:8080/api/appointment/<int:id>/




LIST SERVICE APPOINTMENTS -- GET REQUEST:

 This will return a list of all current service appointments. This is the format that will be displayed:

    {
	"appointments": [
		{
			"date_time": "2024-02-06T18:43:03+00:00",
			"reason": "Flat Tire",
			"status": "Finished",
			"vin": "5NPET4AC5AH57777",
			"customer": "Jose",
			"technician": {
				"first_name": "Hermonie",
				"last_name": "Granger",
				"employee_id": 3,
				"id": 38
			},
			"id": 11
        },
        {
            "date_time": "2024-02-06T18:43:03+00:00",
            "reason": "Broken Windshield",
            "status": "Finished",
            "vin": "5NPET4AC5AH57777",
            "customer": "Peter",
            "technician": {
                "first_name": "Draco",
                "last_name": "Malfoy",
                "employee_id": 4,
                "id": 37
            },
            "id": 12
        },
	]
}

SERVICE APPOINTMENT DETAIL -- GET REQUEST:

This will return the detail of each specific service appointment.

    		{
			"date_time": "2024-02-06T18:43:03+00:00",
			"reason": "Flat Tire",
			"status": "Finished",
			"vin": "5NPET4AC5AH57777",
			"customer": "Jose",
			"technician": {
				"first_name": "Hermonie",
				"last_name": "Granger",
				"employee_id": 3,
				"id": 38
			},
			"id": 11
		},

DELETE SERVICE APPOINTMENT -- DELTE REQUEST:

Input the "id" of the service appointment that you want to delete at the end of the url. For example, if we wanted to delete the above service history appointment for Jose just enter 'http://localhost:8080/api/appointment/11' into the field and send the request.

CREATE SERVICE APPOINTMENT -- POST REQUEST:

 This will create a service appointment with the data input. It must follow the format.

        		{
			"date_time": "2024-02-06T18:43:03+00:00",
			"reason": "Flat Tire",
			"status": "Finished",
			"vin": "5NPET4AC5AH57777",
			"customer": "Jose",
			"technician": {
				"first_name": "Hermonie",
				"last_name": "Granger",
				"employee_id": 3,
				"id": 38
			},
			"id": 11
		},

UPDATE SERVICE APPOINTMENT STATUS -- PUT REQUEST:

This will update the status of an appointment from 'null' or from the cancelled or finished status, to be either 'cancelled' or 'finished: as displayed in this url "http://localhost:8080/api/appointments/8/finish/". Be sure to change the /<intid>/ and /<str:status>/
to the desired appointment 'id' and 'status'.

{
	"date_time": "2024-02-06T18:43:03+00:00",
	"reason": "Flat Tire",
	"status": "Finished",
	"vin": "5NPET4AC5AH57777",
	"customer": "Jose",
	"technician": {
		"first_name": "Draco",
		"last_name": "Malfoy",
		"employee_id": 4,
		"id": 37
	},
	"id": 8
}


*** Technicians ***

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Technician detail | GET | http://localhost:8080/api/technicians/<int:id>/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:id>/


LIST TECHNICIANS -- GET REQUEST:

Following this endpoint will give you a list of all technicians that are currently employed.
{
	"technicians": [
		{
			"first_name": "Ron",
			"last_name": "Weasley",
			"employee_id": 1,
			"id": 32
		},
		{
			"first_name": "Harry",
			"last_name": "Potter",
			"employee_id": 2,
			"id": 34
		},
    ]
}

TECHNICIAN DETAIL--  GET REQUEST:

You will see that they are assigned a value of "id". This is the value that will replace "<int:id>. For example, if you wanted to see the technician
details related to our technician "Ron", you would input the following address: http://localhost:8080/api/technicians/32/
This would then lead to this:

		{
			"first_name": "Ron",
			"last_name": "Weasley",
			"employee_id": 1,
			"id": 32
		},



CREATE TECHNICIAN -- POST REQUEST:

To create a technician, you would use the following format to input the data and you would just submit this as a POST request.
{
	"first_name": "Harry",
    "last_name": "Potter",
	"employee_id": 2
}


DELETE TECHNICIAN - For a request type to "DELETE" instead of "POST". You also need to pull the "id" value just like you did in "TECHNICIAN DETAIL" to make sure you delete the correct one. Once they are "promoted to customer" they will no longer be in our page that lists
all technicians.



################ Sales microservice ################


*** Customer: ***

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/<int:id>/

List OF CUSTOMERS -- GET REQUEST:

 This will return a list of all current customers. This is the format that will be displayed:

{
	"customers": [
		{
			"first_name": "Joan",
			"last_name": "Rivers",
			"address": "123 Imaginary Lane",
			"phone_number": "631-666-6666",
			"id": 2
		},
    ]
}

Customer DETAIL VIEW -- GET REQUEST:

 This will return a list of a customer. This is the format that will be displayed:

{
			"first_name": "Joan",
			"last_name": "Rivers",
			"address": "123 Imaginary Lane",
			"phone_number": "631-666-6666",
			"id": 2
		},


CREATE CUSTOMER - POST REQUEST:

 This will create a customer with the data input. It must follow the format.


{
	"first_name": "Joan",
	"last_name": "Rivers",
	"address": "123 Imaginary Lane",
	"phone_number": "631-666-6666"
}


*** Auto Sales: ***

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all salesrecords | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Delete a sale | DELETE | http://localhost:8090/api/sales/<int:id>/



LIST OF SALES  - GET REQUEST:

 This will return a list of all sales. This is the format that will be displayed:

{
	"sales": [
		{
			"price": 5555555,
			"id": 6,
			"customer_first_name": "Big",
			"customer_last_name": "Spender",
			"customer_address": "321 Money Lane",
			"customer_phone_number": "555-555-555",
			"salesperson_first_name": "Jordan",
			"salesperson_last_name": "Rubenstein",
			"salesperson_employee_id": "516",
			"salesperson_id": 1,
			"vin": "1C3CC5FB2AN120174"
		},
        {
			"price": 10000,
			"id": 7,
			"customer_first_name": "Small",
			"customer_last_name": "Spender",
			"customer_address": "123 Money Gone",
			"customer_phone_number": "123-3456",
			"salesperson_first_name": "Jordan",
			"salesperson_last_name": "Rubenstein",
			"salesperson_employee_id": "516",
			"salesperson_id": 1,
			"vin": "1C3CC5FBHS7FS174"
		},
    ]
}


CREATE A SALE - POST REQUEST:

 This will record and create a sale with the data input. It must follow the format:

{
  "price": 30000,
  "automobile": "33333333333333333",
  "salesperson": 1,
  "customer": 2
}

DELETE A SALE - For a request type to "DELETE" use the "id" value to delete the correct one:
http://localhost:8090/api/sales/<int:id>/


*** Salespeople: ***
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salesperson | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salespeople/<int:id>/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salepeople/<int:id>/



LIST SALESPERSON - GET REQUEST:

 Following this endpoint will give you a list of all sales people:

{
	"salespersons": [
		{
			"first_name": "Jordan",
			"last_name": "Rubenstein",
			"employee_id": "516",
			"id": 1
		},
        {
			"first_name": "Tyler",
			"last_name": "Scott",
			"employee_id": "542",
			"id": 2
		},
    ]
}

CREATE SALESPERSON -- POST REQUEST:

This will create a sales person with the data input. It must follow the format:

{
  "first_name": "Jordan",
  "last_name": "Rubenstein",
  "employee_id": "516"
}

DELETE A SALESPERSON - For a request type to "DELETE" use the "id" value to delete the correct one, not to be confused with the employee_id: http://localhost:8090/api/salespeople/<int:id>/
